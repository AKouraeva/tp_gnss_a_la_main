# -*- coding: utf-8 -*-
"""
@author: BEILIN Jacques - IGN/ENSG
@date  : %(date)s
"""

import numpy as np
import math

import gpsdatetime as gpst


""" Import du module """
# version locale
#import sys
#sys.path.append("/home/beilin/progs/Prog_scientifique/Packages/yagnss_toolbox/python/src/pygnsstoolbox/gnsstoolbox")
#import gnss_process as proc

# version installee
import gnsstoolbox.gnss_process as proc

class Trilat():
    """ Classe trilateration (ou multilateration) """

    def __init__(self,PosSat,Dobs,X0=np.zeros((4,1))):
        self.PosSat = PosSat
        self.Dobs = Dobs
        self.X0 = X0

    def print(self):
        print(self)

    def __str__(self):
        """Ecriture des attributs de la classe point()"""
        out = ''
        for s in self.__dict__:
            out += '%-35s : ' %(s)
            out += str(self.__dict__.get(s)) + "\n"
        return out

    def estimation(self):
        """ Calcul d'une trilatération simple avec un offset
        correspondant à l'erreur d'horloge du récepteur """
        
        
        """Moindres carrés"""
        L = self.Dobs
        
        Kl = (2**2)*np.eye(len(L))
        Ql = Kl
        P = np.linalg.inv(Ql)
        
        A = np.zeros((len(self.PosSat), 4))
        B = np.zeros((len(self.PosSat), 1))
        
        
        for i in range (len(self.PosSat)):
            racine = np.sqrt((self.X0[0]-self.PosSat[i,0])**2 + (self.X0[1]-self.PosSat[i,1])**2 + (self.X0[2]-self.PosSat[i,2])**2)
            A[i,0] = (self.X0[0] - self.PosSat[i,0])/racine
            A[i,1] = (self.X0[1] - self.PosSat[i,1])/racine
            A[i,2] = (self.X0[2] - self.PosSat[i,2])/racine
            A[i,3] = 1
            B[i] = L[i] - racine - self.X0[3]
            
        N = np.transpose(A) @ P @ A
         
        deltax = np.linalg.inv(N) @ np.transpose(A) @ P@ B
        X0 = self.X0
        x_chap = np.transpose(np.asarray([X0])) + deltax
        
        indice = 0

        while indice < 50 :
            for j in range (len(self.PosSat)):
                racine = np.sqrt((X0[0]-self.PosSat[j,0])**2 + (X0[1]-self.PosSat[j,1])**2 + (X0[2]-self.PosSat[j,2])**2)
                A[j,0] = (X0[0] - self.PosSat[j,0])/racine
                A[j,1] = (X0[1] - self.PosSat[j,1])/racine
                A[j,2] = (X0[2] - self.PosSat[j,2])/racine
                A[j,3] = 1
                B[j] = L[j] - racine - self.X0[3]
            N = np.transpose(A) @ P @ A
            X0 = x_chap
            deltax = np.linalg.inv(N) @ np.transpose(A) @ P @ B
            x_chap = X0 + deltax
            indice +=1
        
        
        
        self.Qxx = []
        self.V = []
        self.X = 0
        self.Y = 0
        self.Z = 0
        self.cdtr = 0
        self.sigma0_2 = 0
        
        
        
        self.Qxx = np.linalg.inv(N)
        self.V = B - A @ deltax
        self.X = x_chap[0]
        self.Y = x_chap[1]
        self.Z = x_chap[2]
        self.cdtr = x_chap[3]
        self.sigma0_2 = (np.transpose(self.V) @ P @ self.V)/ (len(L) - len(x_chap))
              
        return 0


if __name__ == "__main__":

    tic = gpst.gpsdatetime()

    PosSat = np.genfromtxt("possat.txt",skip_header=1,delimiter=",")
    Dobs = np.genfromtxt("dobs.txt",skip_header=1,delimiter=",")

    X0=np.array([4202000,178000,4780000,0])

    X,Y,Z,cdtr,sigma0_2,V,SigmaX = proc.trilatGps(PosSat,Dobs,X0)
    print("Solution pygnssToolbox\nX = %13.3f m\nY = %13.3f m\nZ = %13.3f m\ndte = %.9f s" % (X,Y,Z,cdtr))
    print("s0 = ", sigma0_2,"\nV = ", V)
    print("SigmaX", SigmaX)

    T = Trilat(PosSat,Dobs,X0)
    T.estimation()
    print("Fonction developpee lors du TP")
    print("X = %.3f Y = %.3f Z = %.3f dte = %.9f" % (T.X,T.Y,T.Z,T.cdtr))
    print("sigma0_2 = %.3f" % (T.sigma0_2))
    print("V = ",T.V,"\nQxx = ",T.Qxx)

    #print(T)

    toc = gpst.gpsdatetime()
    print ('%.3f sec elapsed ' % (toc-tic))
