# -*- coding: utf-8 -*-
"""
@author: BEILIN Jacques - IGN/ENSG
@date  : %(date)s
"""

import re
import numpy as np
import math
import os

import gpsdatetime as gpst
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

""" Import du module orbits """
# version locale
#import sys
#sys.path.append("/home/beilin/progs/Prog_scientifique/Packages/yagnss_toolbox/python/src/pygnsstoolbox/gnsstoolbox")
#import orbits

# version installee
import gnsstoolbox.orbits as orbits


class orbit_TP(orbits.orbit):
    """ Classe orbits à surcharger.
    Elle hérite de la classe orbit définie dans le module orbits.py """

    def print(self):
        """ Ecriture des attributs """
        for s in self.__dict__:
            print('%-35s : ' %(s), self.__dict__.get(s))

    def pos_sat_sp3(self,const, prn, mjd, ordre):
        """ Calcul de la postion du satellite "const/prn"
        à un instant donné mjd """
        X_ECEF, Y_ECEF, ZECEF, dte = 0,0,0,0

        """ A COMPLETER (TP5) """
        (orb,nl) = self.getSp3(const,prn)
        
        temps = orb[:,0]
        indice = 0
        for i in range (len(temps)):
            if mjd < temps[i]:
                indice = i
                break

        L = []
        yt = []
        num = 0
        den = 0
        
        if (indice-int(ordre/2) < 0):
            while indice-int(ordre/2) < 0 :
                indice += 1
        elif (indice+int(ordre/2)+1 > len(orb)):
            while indice+int(ordre/2)+1 > len(orb) :
                indice -= 1   
            
        for j in range (indice-int(ordre/2), indice+int(ordre/2)+1):
            Lj = 1
            for k in range(indice-int(ordre/2), indice+int(ordre/2)+1):
                if j != k:
                    num = mjd - orb[k,0]
                    den = orb[j,0] - orb[k,0]
                    Lj *= num/den
            L.append(Lj)
              
        yt = L @ orb[indice-int(ordre/2) : indice+int(ordre/2)+1, 1:]
            
        X_ECEF, Y_ECEF, ZECEF, dte = yt[0]*1e3, yt[1]*1e3, yt[2]*1e3, yt[3]*1e-6
        
        return X_ECEF, Y_ECEF, ZECEF, dte

if __name__ == "__main__":

    tic = gpst.gpsdatetime()

    mysp3 = orbit_TP()

    dir_orb = '../data'
    mysp3.loadSp3(os.path.join(dir_orb,'GFZ0OPSULT_20212400600_02D_05M_ORB.SP3'))

    """ Definition de l'instant pour lequel on cherche une position """
    t = gpst.gpsdatetime(yyyy=2021,doy=240,dsec=5435.000)
#    print(t)

    """ SATELLITE """
    constellation = 'G'
    prn = 14

    """ Ordre du polynome """
    ordre = 9

    """ Calcul avec la fonction integree a la toolbox """
    (X_sp3,Y_sp3,Z_sp3,dte_sp3)	 = mysp3.calcSatCoordSp3(constellation,prn,t.mjd,ordre)
    print("Solution pygnssToolbox\nX = %13.3f m\nY = %13.3f m\nZ = %13.3f m\ndte = %.9f s" % (X_sp3,Y_sp3,Z_sp3,dte_sp3))
    dt = 0.001
    (X1,Y1,Z1,dte1)	= mysp3.calcSatCoordSp3(constellation,prn,t.mjd-dt/86400,ordre)
    (X2,Y2,Z2,dte2)	= mysp3.calcSatCoordSp3(constellation,prn,t.mjd+dt/86400,ordre)
    V = np.array([[X2-X1],[Y2-Y1],[Z2-Z1]]) / 2 / dt
    print("V = %.3f m/s" % np.linalg.norm(V))

    """ Fonction developpee lors du TP """
    X,Y,Z,dte = mysp3.pos_sat_sp3(constellation,prn,t.mjd,ordre)
    X1,Y1,Z1,dte1 = mysp3.pos_sat_sp3(constellation,prn,t.mjd-dt/86400,ordre)
    X2,Y2,Z2,dte2 = mysp3.pos_sat_sp3(constellation,prn,t.mjd+dt/86400,ordre)
    V = np.array([[X2-X1],[Y2-Y1],[Z2-Z1]]) / 2 / dt
    print("Fonction developpee lors du TP\nX = %13.3f m\nY = %13.3f m\nZ = %13.3f m\ndte = %.9f s" % (X1,Y1,Z1,dte1))
    print("V = %.3f m/s" % np.linalg.norm(V))
    
#    coord = np.zeros((4, 4))
#    for i in range(1, 6):
#        if 2*i+1 < 9:
#            X1,Y1,Z1,dte1 = mysp3.pos_sat_sp3(constellation,prn,t.mjd,2*i+1)
#            coord[i-1,:] = [X,Y,Z,dte]
#        elif 2*i+1 > 9:
#            X1,Y1,Z1,dte1 = mysp3.pos_sat_sp3(constellation,prn,t.mjd,2*i+1)
#            coord[i-2,:] = [X,Y,Z,dte]
            
#    
#            
#    orb3 = mysp3.getSp3(constellation,3)
#    orb9 = mysp3.getSp3(constellation,9)
#    
#    
#    fig = plt.figure()
#    ax = fig.gca(projection = "3d")
#    
#    
#    
#    ax.plot(orb9[0][:,0], orb9[0][:,1], orb9[0][:,2], label='parametric curve')
#    ax.legend()
#    
#    plt.show()

    
#    ax.plot(orb3[:,0], orb3[:,1], orb3[:,2])
#    ax.plot(orb9[:,0], orb9[:,1], orb9[:,2])
#    

    toc = gpst.gpsdatetime()
    print ('%.3f sec elapsed ' % (toc-tic))

