# -*- coding: utf-8 -*-
"""
Created on %(date)s

@author: %(username)s
"""
import re
import os
import numpy as np
import gpsdatetime as gpst
import math
import pip

""" Import du module orbits """
# version locale
#import sys
#sys.path.append("/home/beilin/progs/Prog_scientifique/Packages/yagnss_toolbox/python/src/pygnsstoolbox/gnsstoolbox")
#import orbits

# version installee
import gnsstoolbox.orbits as orbits

class orbit_TP(orbits.orbit):
    """ Classe orbits à surcharger.
    Elle hérite de la classe orbit définie dans le module orbits.py """

    def print(self):
        """ Ecriture des attributs """
        for s in self.__dict__:
            print('%-35s : ' %(s), self.__dict__.get(s))


    def pos_sat_brdc(self,const,prn,temps):
        """ Calcul de la postion du satellite "const/prn"
        à un instant donné mjd """
        mjd = temps.mjd
        X_ECEF, Y_ECEF, Z_ECEF, dte = 0,0,0,0

        """ A COMPLETER (TP3) """
        Eph = self.getEphemeris(const,prn,mjd)
        print("TOE : ", Eph.mjd)
        
#        t = gpst.gpsdatetime(mjd=mjd)
        
        Deltat = (mjd - Eph.mjd) * 86400
        print("delta_t ", Deltat)
        
        # Moyen mouvement
        deltaN = Eph.delta_n
        mu = 3.986005*10**14
        n0 = np.sqrt(mu/Eph.sqrt_a**6)
        n = n0 + deltaN
        print("n =", n)
        
        # Anomalie moyenne
        m0 = Eph.M0
        M = m0 + n*Deltat
        print("M = ", M)
        
        # Equation de Kepler
        Ek = M
        ex = Eph.e
        Ekp1 = M + ex*np.sin(Ek)
        c = 1
        while (Ekp1 - Ek)*Eph.sqrt_a**2 > 10**(-2):
            Ek = Ekp1
            Ekp1 = M + ex*np.sin(Ek)
            c+=1
        print("Ek = ", Ek)
        print("Iteration = ", c)
        
        # Anomalie vraie
        
        v = 2*math.atan(np.sqrt((1 + Eph.e)/(1 - Eph.e)) * np.tan(Ek/2))
        print("v = ", v)
        
        # Rayon
        
        r = (Eph.sqrt_a**2)*(1 - Eph.e*np.cos (Ek))
        print("r = ", r)
        
        # Coord orbitales
        phi = Eph.omega + v
        print("phi = ",phi)
        
        dphi    = Eph.cus * np.sin(2*phi) + Eph.cuc * np.cos(2*phi)
        dr      = Eph.crs * np.sin(2*phi) + Eph.crc * np.cos(2*phi)
        print("dphi = ",dphi)
        print("dr = ",dr)
        
        # Calcul x, y 
        x = (r + dr)*np.cos(phi + dphi)
        y = (r + dr)*np.sin(phi + dphi)
        print("x = ",x)
        print("y = ",y)
        
        # Positions orbitales
        di  = Eph.cis*np.sin(2*phi) + Eph.cic*np.cos(2*phi)
        i   = Eph.i0 + Eph.IDOT * Deltat + di
        Om = Eph.OMEGA + Eph.OMEGA_DOT* Deltat
        print("i = ",i)
        print("Om = ", Om)
        
        # Coord cartésiennes géocentriques
        mat1 = np.array([[np.cos(Om), -np.sin(Om), 0], [np.sin(Om), np.cos(Om), 0], [0, 0, 1]])
        mat2 = np.array([[1, 0, 0], [0, np.cos(i), -np.sin(i)], [0, np.sin(i), np.cos(i)]])
        X1 = np.array([[x], [y], [0]])
        Xec1 = mat1 @ mat2 @ X1
        
        print("Xec1 = ", Xec1)
        
        # Coord cartésiennes ECEF
        Ome = (-7.2921151467*10**(-5))
        print("Ome = ", Ome)
        mat3 = np.array([[np.cos(Ome*t.wsec), -np.sin(Ome*t.wsec), 0], [np.sin(Ome*t.wsec), np.cos(Ome*t.wsec), 0], [0,0,1]])
        Xecef = mat3 @ Xec1
        
        print("Xecef = ", Xecef)
        
        


        print("demi-grand axe a=%.3fm" % (Eph.sqrt_a**2))
        print(Eph.mjd, mjd)

        return X_ECEF, Y_ECEF, Z_ECEF, dte


if __name__ == "__main__":

    tic = gpst.gpsdatetime()

    Orb = orbit_TP()

    """ lecture du fichier BRDC """
    dir_orb = '../data'
    Orb.loadRinexN(os.path.join(dir_orb, 'MLVL00FRA_R_20212400000_01D_GN.rnx'))

    """ Definition de l'instant pour lequel on cherche une position """
    t = gpst.gpsdatetime(yyyy=2021,doy=240,dsec=5435)
    print(t)

    """ SATELLITE """
    constellation = 'G'
    prn = 14

    try:
        Eph = Orb.getEphemeris(constellation,prn,t.mjd)
        print(Eph)
        print("TOC : ",Eph.tgps.st_iso_epoch())
             
    except:
        print("Unable to find satellite !!! ")


    print(Eph.sqrt_a**2,Eph.e)

    """ Calcul avec la fonction integee a la toolbox """
    X,Y,Z,dte = Orb.calcSatCoord(constellation,prn,t.mjd) 
    print("Solution pygnssToolbox\nX = %13.3f m\nY = %13.3f m\nZ = %13.3f m\ndte = %.9f s" % (X,Y,Z,dte))
    
    """ impression des elements de debug """
    print(Orb.debug.__dict__)

#    print(">>>>>>>>>>>>>",Orb.debug.X_ECI[0].squeeze())

    """ Calcul avec la fonction integee developpee lors du TP """
    X,Y,Z,dte = Orb.pos_sat_brdc(constellation,prn,t)
    print("X = %.3f Y = %.3f Z = %.3f dte = %.9f" % (X,Y,Z,dte))

###    print(Orb.debug.__dict__)

    toc = gpst.gpsdatetime()
    print ('%.3f sec elapsed ' % (toc-tic))

