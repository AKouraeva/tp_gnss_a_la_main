# -*- coding: utf-8 -*-
"""
@author: BEILIN Jacques - IGN/ENSG
@date  : %(date)s
"""

import re
import numpy as np
import math

import gpsdatetime as gpst

""" Import du module """
# version locale
#import sys
#sys.path.append("/home/beilin/progs/Prog_scientifique/Packages/yagnss_toolbox/python/src/pygnsstoolbox/gnsstoolbox")
#import gnss_process as proc

# version installee
import gnsstoolbox.gnss_process as proc

class Trilat():
    """ Classe trilateration (ou multilateration) """

    def __init__(self,PosSat,Dobs,ElevSat,X0=np.zeros((4,1))):
        self.PosSat = PosSat
        self.Dobs = Dobs
        self.X0 = X0
        self.ElevSat = ElevSat

    def print(self):
        """Ecriture des attributs de la classe point()"""
        for s in self.__dict__:
            print('%-35s : ' %(s), self.__dict__.get(s))

    def estimation(self):
        """ Calcul d'une trilatération simple avec un offset
        correspondant à l'erreur d'horloge du récepteur """
        

        L = self.Dobs
        

        sigma0 = 2
        Kl = np.eye(len(L))
        for i in range(len(L)):
            Kl[i,i] = (sigma0/math.sin(self.ElevSat[i]))**2
        Ql = Kl
        P = np.linalg.inv(Ql)
        
        A = np.zeros((len(self.PosSat),4))
        B = np.zeros((len(self.PosSat), 1))

        
        for i in range(len(self.PosSat)):
            racine = np.sqrt((self.X0[0]-self.PosSat[i,0])**2 + (self.X0[1]-self.PosSat[i,1])**2 + (self.X0[2]-self.PosSat[i,2])**2)
            A[i,0] = (self.X0[0]-self.PosSat[i,0])/racine
            A[i,1] = (self.X0[1]-self.PosSat[i,1])/racine
            A[i,2] = (self.X0[2]-self.PosSat[i,2])/racine
            A[i,3] = 1
            

            
            B [i] = L[i] - racine - self.X0[3]
        

  
        N = A.T @ P @ A
        mat_ = A.T @ P @ B
        delta_x = np.linalg.inv(N) @ mat_

        X0 = self.X0

        x_hat = np.reshape([X0], (4,1)) + delta_x
        print('x_hat =',x_hat)
        
        
#        print("avant boucle ",A)
        for i in range(10):
   
            
            X0 = x_hat
            for j in range(len(self.PosSat)):
             
                racine = np.sqrt((X0[0]-self.PosSat[j,0])**2 + (X0[1]-self.PosSat[j,1])**2 + (X0[2]-self.PosSat[j,2])**2)
                A[j,0] = (X0[0]-self.PosSat[j,0])/racine
                A[j,1] = (X0[1]-self.PosSat[j,1])/racine
                A[j,2] = (X0[2]-self.PosSat[j,2])/racine
                A[j,3] = 1
                
            
                B[j] = L[j] - racine - X0[3]
                

            N = A.T @ P @ A
#            print("après boucle ",A)
            delta_x = np.linalg.inv(N) @ A.T @ P @ B
            x_hat = X0 + delta_x
            v= B - A @ delta_x
            sigma0_2 = (V.T @ P @ V)/ (len(L)-len(x_hat)) 
   
        

        print("PosSat = ",self.PosSat)

        self.Qxx = np.linalg.inv(N)
        self.V = v
        self.V_norm = self.V/sigma0_2
        self.X = x_hat[0]
        self.Y = x_hat[1]
        self.Z = x_hat[2]
        self.cdtr = x_hat[3]
        self.sigma0_2 = sigma0_2
        return 0


if __name__ == "__main__":

    tic = gpst.gpsdatetime()

    PosSat = np.genfromtxt("possat.txt",skip_header=1,delimiter=",")
    Dobs = np.genfromtxt("dobs.txt",skip_header=1,delimiter=",")
    ElevSat = np.genfromtxt("ElevSat.txt",skip_header=1,delimiter=",")
    X0=np.array([4202000,178000,4780000,0])
    satIndex = np.ones(Dobs.shape)

    X,Y,Z,cdtr,cGGTP, cGPGL, sigma0_2_p,V,SigmaX = proc.trilatGnssPonderationElev(PosSat,Dobs,X0,satIndex,ElevSat)
    print("Solution pygnssToolbox\nX = %13.3f m\nY = %13.3f m\nZ = %13.3f m\ndte = %.9f s" % (X,Y,Z,cdtr))
    print("s0 = ", sigma0_2_p,"\nV = ", V)
    print("SigmaX", SigmaX)


    T = Trilat(PosSat,Dobs,ElevSat,X0)
    T.estimation()
    print("Fonction developpee lors du TP")
    print("X = %.3f Y = %.3f Z = %.3f cdtr = %.9f" % (T.X,T.Y,T.Z,T.cdtr))
#    print("cGGTO = %.9f cGPGL = %.9f" % (T.cGGTO,T.cGPGL))
    print("sigma0_2 = %.3f" % (T.sigma0_2))
    print("V = ",T.V,"\nQxx = ",T.Qxx, "\n Vnorm = ", T.V_norm)

    toc = gpst.gpsdatetime()
    print ('%.3f sec elapsed ' % (toc-tic))
