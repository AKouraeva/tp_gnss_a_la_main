# -*- coding: utf-8 -*-
"""
@author: BEILIN Jacques - IGN/ENSG
@date  : %(date)s
"""

import numpy as np
import math
import gpsdatetime as gpst

""" Import du module """
# version locale
#import sys
#sys.path.append("/home/beilin/progs/Prog_scientifique/Packages/yagnss_toolbox/python/src/pygnsstoolbox/gnsstoolbox")
#import gnss_process as proc

# version installee
import gnsstoolbox.gnss_process as proc


class Trilat():
    """ Classe trilateration (ou multilateration) """
    
    def __init__(self,PosSat,Dobs,SatIndex,ElevSat,X0=np.zeros((4,1))):
        self.PosSat = PosSat
        self.Dobs = Dobs
        self.X0 = X0
        self.SatIndex = SatIndex
        self.ElevSat = ElevSat
    
    def print(self):
        """Ecriture des attributs de la classe point()"""
        for s in self.__dict__:
            print('%-35s : ' %(s), self.__dict__.get(s))

    def estimation(self):
        """ Calcul d'une trilatération simple avec un offset
        correspondant à l'erreur d'horloge du récepteur
        Et 2 offsets correspondant aux décallages Glonass et Galileo """

        L = self.Dobs
        
        s_code = 2
        
        Kl = np.eye(len(L))
        for sigma in range (len(Kl)):
            Kl[sigma, sigma] =  s_code / np.sin(self.ElevSat[sigma])
        
        Ql = Kl
        P = np.linalg.inv(Ql)
        
        A = np.zeros((len(PosSat), 6))
        B = np.zeros((len(PosSat), 1))
        
        
        X0 = np.zeros((6,1))
        X0 =  self.X0[0], self.X0[1], self.X0[2], self.X0[3],0,0
        
        if 
            for i in range (len(self.PosSat)):
                racine = np.sqrt((self.X0[0]-PosSat[i,0])**2 + (self.X0[1]-PosSat[i,1])**2 + (self.X0[2]-PosSat[i,2])**2)
                A[i,0] = (X0[0] - PosSat[i,0])/racine
                A[i,1] = (X0[1] - PosSat[i,1])/racine
                A[i,2] = (X0[2] - PosSat[i,2])/racine
                A[i,3] = 1
                B[i] = L[i] - racine - X0[3]
        if         
            for i in range (len(self.PosSat)):
                racine = np.sqrt((self.X0[0]-PosSat[i,0])**2 + (self.X0[1]-PosSat[i,1])**2 + (self.X0[2]-PosSat[i,2])**2)
                A[i,0] = (X0[0] - PosSat[i,0])/racine
                A[i,1] = (X0[1] - PosSat[i,1])/racine
                A[i,2] = (X0[2] - PosSat[i,2])/racine
                A[i,3] = 1
            B[i] = L[i] - racine - X0[3]
            
        N = np.transpose(A) @ P @ A
         
        deltax = np.linalg.inv(N) @ np.transpose(A) @ P@ B
        x_chap = np.transpose(np.asarray([X0])) + deltax
        
        indice = 0

        while indice < 50 :
            for j in range (len(self.PosSat)):
                racine = np.sqrt((X0[0]-PosSat[j,0])**2 + (X0[1]-PosSat[j,1])**2 + (X0[2]-PosSat[j,2])**2)
                A[j,0] = (X0[0] - PosSat[j,0])/racine
                A[j,1] = (X0[1] - PosSat[j,1])/racine
                A[j,2] = (X0[2] - PosSat[j,2])/racine
                A[j,3] = 1
                B[j] = L[j] - racine - X0[3]
            N = np.transpose(A) @ P @ A
            X0 = x_chap
            deltax = np.linalg.inv(N) @ np.transpose(A) @ P @ B
            x_chap = X0 + deltax
            indice +=1
        
        
        
        self.Qxx = []
        self.V = []
        self.X = 0
        self.Y = 0
        self.Z = 0
        self.cdtr = 0
        self.sigma0_2 = 0
        
        self.Qxx = np.linalg.inv(N)
        self.V = B - A @ deltax
        self.X = x_chap[0]
        self.Y = x_chap[1]
        self.Z = x_chap[2]
        self.cdtr = x_chap[3]
        self.sigma0_2 = (np.transpose(self.V) @ P @ self.V)/ (len(L) - len(x_chap))
        self.cGGTO = 0
        self.cGPGL = 0
        return 0

if __name__ == "__main__":

    tic = gpst.gpsdatetime()

    PosSat = np.genfromtxt("possat.txt",skip_header=1,delimiter=",")  
    Dobs = np.genfromtxt("dobs.txt",skip_header=1,delimiter=",")
    SatIndex = np.genfromtxt("sat_index.txt",skip_header=1,delimiter=",")
    ElevSat = np.genfromtxt("ElevSat.txt",skip_header=1,delimiter=",")
    X0=np.array([4202000,178000,4780000,0]) 
    
    X,Y,Z,cdtr,cGGTO, cGPGL, sigma0_2,V,SigmaX = proc.trilatGnssPonderationElev(PosSat,Dobs,X0,SatIndex,ElevSat)
    print("Solution pygnssToolbox\nX = %13.3f m\nY = %13.3f m\nZ = %13.3f m\ndte = %.9f s" % (X,Y,Z,cdtr))
    print("GGTO =",cGGTO, "\nGPGL =",cGPGL)
    print("s0 =", sigma0_2,"\nV =", V)
    print("SigmaX", SigmaX)

    T = Trilat(PosSat,Dobs,SatIndex,ElevSat,X0)
    T.estimation()
    print("Fonction developpee lors du TP")
    print("X = %.3f Y = %.3f Z = %.3f cdtr = %.9f" % (T.X,T.Y,T.Z,T.cdtr))
    print("cGGTO = %.9f cGPGL = %.9f" % (T.cGGTO,T.cGPGL))
    print("sigma0_2 = %.3f" % (T.sigma0_2))
    print("V = ",T.V,"\nQxx = ",T.Qxx)
#    
#    toc = gpst.gpsdatetime()
#    print ('%.3f sec elapsed ' % (toc-tic))
